from .frame import HF_Frame, hf_frame_data, opcodes, opnames
from .frame import lebytes_to_int, int_to_lebytes

class HF_OP_POWER(HF_Frame):
  def __init__(self, bytes=None, power=0x1):
    if bytes is None:
      # REQUEST
      HF_Frame.__init__(self,{'operation_code':   opcodes['OP_POWER'],
                              'chip_address':     0xFF,
                              'core_address':     0x00,
                              'hdata':            power }) # 0x1 diagnostic_power_on, 0x2 diagnostic_power_off
      self.construct_framebytes()
    else:
      # READ
      HF_Frame.__init__(self, bytes)

  def __str__(self):
    string  = "HF_OP_POWER\n"
    string += HF_Frame.__str__(self)
    return string