from .frame import HF_Frame, opcodes, opnames
from .frame import lebytes_to_int, int_to_lebytes

class HF_OP_USB_SHUTDOWN(HF_Frame):
  def __init__(self, bytes=None):
    if bytes is None:
      HF_Frame.__init__(self,{'operation_code': opcodes['OP_USB_SHUTDOWN'],
                              'hdata'         : 2 })
    else:
      HF_Frame.__init__(self, bytes)