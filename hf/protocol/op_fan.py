from .frame import HF_Frame, hf_frame_data, opcodes, opnames
from .frame import lebytes_to_int, int_to_lebytes

class HF_OP_FAN(HF_Frame):
  def __init__(self, bytes=None, speed=10):
    if bytes is None:
      assert speed <= 99
      HF_Frame.__init__(self,{'operation_code': opcodes['OP_FAN'],
                              'core_address':   0x01,
                              'chip_address':   0xFF,
                              'hdata':          int(speed*256/100) })
    else:
      HF_Frame.__init__(self, bytes)

  def __str__(self):
    string  = "OP_FAN\n"
    string += HF_Frame.__str__(self)
    return string