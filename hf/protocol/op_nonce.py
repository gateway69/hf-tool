from .frame import HF_Frame, opcodes, opnames
from .frame import lebytes_to_int, int_to_lebytes

# From hf_protocol.h
HF_NTIME_MASK = 0x0fff       # Mask for for ntime
# If this bit is set, search forward for other nonce(s)
HF_NONCE_SEARCH = 0x1000     # Search bit in candidate_nonce -> ntime

# Imitates "strudct hf_candidate_nonce" in hf_protocols.h.
class hf_candidate_nonce:
  def __init__(self, nonce_bytes):
    assert len(nonce_bytes) == 8
    self.nonce = lebytes_to_int(nonce_bytes[0:4])
    self.sequence = lebytes_to_int(nonce_bytes[4:6])
    self.ntime = lebytes_to_int(nonce_bytes[6:8])
    self.ntime_offset = self.ntime & HF_NTIME_MASK
    self.search_forward = self.ntime & HF_NONCE_SEARCH

class HF_OP_NONCE(HF_Frame):
  def __init__(self, framebytes):
    HF_Frame.__init__(self, framebytes)
    assert len(self.data) % 8 == 0
    self.nonces = []
    for i in range(int(len(self.data) / 8)):
      self.nonces = self.nonces + [hf_candidate_nonce(self.data[8*i:8*i+8])]