from .frame import HF_Frame, opcodes, opnames
from .frame import lebytes_to_int, int_to_lebytes


# Modeled on struct hf_usb_notice_data in hf_protocol.h.
class HF_OP_USB_NOTICE(HF_Frame):
  def __init__(self, initial_state):
    HF_Frame.__init__(self, initial_state)
    self.notification_code = self.hdata
    self.extra_data = None
    self.message = None
    if self.data_length_field > 0:
      self.extra_data = lebytes_to_int(self.data[0:4])
    if self.data_length_field > 1:
      try:
        raw_message = self.data[4:]
        first_NUL = raw_message.index(0)
      except ValueError:
        # Fix: Check that the last bytes are all NUL, there may be more than
        #      one, once the firmware is fixed to do that.
        raise HF_Error("OP_USB_NOTICE returned a non-NUL terminated string.")
      self.message = "".join([chr(x) for x in raw_message[0:first_NUL]])