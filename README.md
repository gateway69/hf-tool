The hftool.py utility is used to query and manipulate hashfast module settings.

This has been tested on Ubuntu 13.x 64bit.

**Requirements:** 

*  Python 3.x
*  pip for Python 3.x
*  Pyusb

**Installing pip for python 3 on most unix systems (command for ubuntu below)**


```
#!python

sudo apt-get install python3-pip
```


**hftool requires pyusb.  To install it:**


```
#!python

pip3 install --pre pyusb
```


**Put these into a file /etc/udev/rules.d/01-hashfast.rules:**

You will need root access to create this file and also when done its recommended you reboot
in order for these rules to take effect.


```
#!python

ATTRS{idVendor}=="297c", ATTRS{idProduct}=="0001", SUBSYSTEMS=="usb", ACTION=="add", MODE="0660", GROUP="plugdev”, ENV{ID_MM_DEVICE_IGNORE}="1"
ATTRS{idVendor}=="297c", ATTRS{idProduct}=="8001", SUBSYSTEMS=="usb", ACTION=="add", MODE="0660", GROUP="plugdev", ENV{ID_MM_DEVICE_IGNORE}="1"
ATTRS{idVendor}=="03eb", ATTRS{idProduct}=="2ff6", SUBSYSTEMS=="usb", ACTION=="add", MODE="0660", GROUP="plugdev", ENV{ID_MM_DEVICE_IGNORE}="1"
```


RUNNING
===========

**Start the tool by with:**

```
#!python

$ ./hftool.py -h
```


**Read die frequencies and voltages:**

```
#!python

$ ./hftool.py -r
```


**Write die frequencies and settings:**

```
#!python

$ ./hftool.py -w 0:VLT@FRQ,1:VLT@FRQ,2:VLT@FRQ,3:VLT@FRQ
```


example would be setting all dies to .930 volts at 875Mhz


```
#!python

$ ./hftool.py -w 0:930@875,1:930@875,2:930@875,3:930@875
```



Copyright (C) 2014, HashFast Technologies LLC | All rights reserved.