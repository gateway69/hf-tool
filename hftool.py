#! /usr/bin/env python3

# Copyright (c) 2014 HashFast Technologies LLC

# Running on Debian (sudo may be required):
#   apt-get update
#   apt-get install python-pip
#   pip install --pre pyusb
#   ./ctrltest.py

import sys
import getopt
from hf.usb import usbbulk
from hf.load import talkusb
from hf.load.routines import settings

def main(argv):
  #
  # usage
  #
  usage  = "usage: hcm.py\n"
  usage += "    -r                          read die settings\n"
  usage += "    -w <<die>:<vlt>@<frq>>,<..> write die settings\n"
  # 
  # get opt
  #
  try:
    opts, args = getopt.getopt(argv,"hrw:")
  except getopt.GetoptError:
    print (usage)
    sys.exit(2)
  # 
  # query device
  #
  dev = usbbulk.poll_hf_bulk_device()
  print (dev.info())
  print (dev.init())

  # Fix: talkusb patch
  talkusb.epr = dev.epr
  talkusb.epw = dev.epw
  #talkusb.talkusb(hf.INIT, None, 0)
  setter = settings.SettingsRoutine(talkusb.talkusb, 1, print)


  #
  # parse args
  #
  for opt, arg in opts:
    if   opt == '-h':
      print (usage)
      sys.exit()
    elif opt == '-r':
      setter.global_state = 'read'
      while setter.one_cycle():
        pass
    elif opt == '-w':
      die_settings = arg.split(',')
      for die_setting in die_settings:
        die, setting = die_setting.split(':')
        vlt, frq     = setting.split('@')
        setter.setup(int(die), int(frq), int(vlt))
      while setter.one_cycle():
        pass

if __name__ == "__main__":
   main(sys.argv[1:])
